import { Authorized, Body, JsonController, Post, Req } from 'routing-controllers';
import { OpenAPI } from 'routing-controllers-openapi';

import { AuthenticatedUser } from '../../auth/AuthenticatedUser';
import { User } from '../models/User/UserModel';
import { TokenService } from '../services/TokenService';
import { UserService } from '../services/UserService';

@Authorized()
@OpenAPI({security: [{ ApiKeyAuth: [] }], summary: 'authorization controller'})
@JsonController('/authenticate')
export class AuthController {

    constructor(
        private userService: UserService,
        private tokenService: TokenService
    ) { }

    @Post()
    public async oAuth(@Req() req: any, @Body() body: { firstName: string, lastName: string, email: string, provider: string }):
        Promise<AuthenticatedUser> {
        const email = body.email;
        let user = await this.userService.findOneByOption({ where: { email } });
        if (!user) {
            user = await this.userService.create(new AuthenticatedUser(), new User(body.firstName, body.lastName, body.email, body.email));
        }
        const token = await this.tokenService.refreshAndGetTokenForUser(user);
        return Promise.resolve(user.toAuthenticatedUser(req, token));
    }
}
