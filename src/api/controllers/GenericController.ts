import { validate, ValidationError } from 'class-validator';
import { BadRequestError } from 'routing-controllers';

import { AuthenticatedUser } from '../../auth/AuthenticatedUser';
import { GenericEntity } from '../models/GenericEntity/GenericEntity';
import { OrmQueryBody } from '../models/Query/QueryBody';
import { OrmQueryParam } from '../models/Query/QueryParam';
import { GenericEntityService } from '../services/GenericEntityService';

export abstract class GenericController<T extends GenericEntity> {

    constructor(
        private service: GenericEntityService<T>
    ) { }

    /**
     *
     *
     * @param {OrmQueryParam} searchParam
     * @returns {Promise<T[]>}
     * @memberof GenericController
     */
    public find(searchParam: OrmQueryParam): Promise<T[]> {
        return this.service.find(OrmQueryParam.toORMQuery(searchParam));
    }

    /**
     *
     *
     * @param {OrmQueryParam} searchParam
     * @param {OrmQueryBody} searchObject
     * @returns {Promise<T[]>}
     * @memberof GenericController
     */
    public findBySearchObject(searchParam: OrmQueryParam, searchObject: OrmQueryBody): Promise<T[]> {
        return this.service.find({...OrmQueryParam.toORMQuery(searchParam), ...OrmQueryBody.toORMQuery(searchObject)});
    }

    /**
     *
     *
     * @param {number} id
     * @returns {(Promise<T | undefined>)}
     * @memberof GenericController
     */
    public one(id: number): Promise<T | undefined> {
        return this.service.findOne(id);
    }

    /**
     *
     *
     * @param {AuthenticatedUser} authUser
     * @param {T} entity
     * @returns {Promise<T>}
     * @memberof GenericController
     */
    public create(authUser: AuthenticatedUser, entity: T): Promise<T> {
        validate(entity).catch(errors => {
            throw new BadRequestError(errors);
        });
        return this.service.create(authUser, entity);
    }

    /**
     *
     *
     * @param {number} id
     * @param {T} entity
     * @returns {(Promise<T | undefined>)}
     * @memberof GenericController
     */
    public update(authUser: AuthenticatedUser, id: number, entity: T): Promise<T | undefined> {
        validate(entity).catch(errors => {
            throw new BadRequestError(errors);
        });
        return this.service.update(authUser, id, entity);
    }

    /**
     *
     *
     * @param {number} id
     * @returns {Promise<void>}
     * @memberof GenericController
     */
    public delete(authUser: AuthenticatedUser, id: number): Promise<void> {
        return this.service.delete(authUser, id);
    }

    /**
     *
     *
     * @param {GenericEntity} entity
     * @returns {Promise<ValidationError[]>}
     * @memberof GenericController
     */
    public validateBody(entity: GenericEntity): Promise<ValidationError[]> {
        return validate(entity);
    }

}
