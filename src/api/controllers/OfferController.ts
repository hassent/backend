import {
    Authorized, Body, CurrentUser, Delete, Get, JsonController, OnUndefined, Param, Post, Put,
    QueryParam, Req
} from 'routing-controllers';
import { OpenAPI } from 'routing-controllers-openapi';

import { AuthenticatedUser } from '../../auth/AuthenticatedUser';
import { EntityNotFoundError } from '../errors/EntityNotFoundError';
import { Offer } from '../models/Offer/OfferModel';
import { OrmQueryBody } from '../models/Query/QueryBody';
import { OrmQueryParam } from '../models/Query/QueryParam';
import { Requirement } from '../models/Requirement/RequirementModel';
import { Reward } from '../models/Reward/RewardModel';
import { OfferService } from '../services/OfferService';
import { GenericController } from './GenericController';

@Authorized()
@OpenAPI({security: [{ ApiKeyAuth: [] }], summary: 'offer controller'})
@JsonController('/offers')
export class OfferController extends GenericController<Offer> {

    constructor(
        private offerService: OfferService
    ) {  super(offerService); }

    @Get()
    public find(@QueryParam('searchParam') searchParam: OrmQueryParam): Promise<Offer[]> {
        return super.find(searchParam);
    }

    @Post('/search')
    public findBySearchObject(@QueryParam('searchParam') searchParam: OrmQueryParam, @Body() searchObject: OrmQueryBody): Promise<Offer[]> {
        return super.findBySearchObject(searchParam, searchObject);
    }

    @Get('/:id')
    @OnUndefined(EntityNotFoundError)
    public one(@Param('id') id: number): Promise<Offer | undefined> {
        return super.one(id);
    }

    @Post()
    public create(@CurrentUser({required: true}) authUser: AuthenticatedUser, @Body() entity: Offer): Promise<Offer> {
        return super.create(authUser, entity);
    }

    @Put('/:id')
    @OnUndefined(EntityNotFoundError)
    public update(@CurrentUser({required: true}) authUser: AuthenticatedUser, @Param('id') id: number, @Body() entity: Offer): Promise<Offer | undefined> {
        return super.update(authUser, id, entity);
    }

    @Delete('/:id')
    public delete(@CurrentUser({required: true}) authUser: AuthenticatedUser, @Param('id') id: number): Promise<void> {
        return super.delete(authUser, id);
    }
    /* Relations */
    @Authorized('view.self.requirement')
    @Get('/:id/requirements')
    public requirements(@Param('id') id: number): Promise<Requirement[]> {
        return this.offerService.findRequirements(id);
    }

    @Get('/:id/rewards')
    public rewards(@Param('id') id: number): Promise<Reward[]> {
        return this.offerService.findRewards(id);
    }
}
