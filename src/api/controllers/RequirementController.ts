import {
    Authorized, Body, CurrentUser, Delete, Get, JsonController, OnUndefined, Param, Post, Put,
    QueryParam, Req
} from 'routing-controllers';
import { OpenAPI } from 'routing-controllers-openapi';

import { AuthenticatedUser } from '../../auth/AuthenticatedUser';
import { EntityNotFoundError } from '../errors/EntityNotFoundError';
import { Offer } from '../models/Offer/OfferModel';
import { OrmQueryBody } from '../models/Query/QueryBody';
import { OrmQueryParam } from '../models/Query/QueryParam';
import { RequirementEvent } from '../models/Requirement/RequirementEventModel';
import { Requirement } from '../models/Requirement/RequirementModel';
import { RequirementService } from '../services/RequirementService';
import { GenericController } from './GenericController';

/**
 *
 *
 * @export
 * @class RequirementController
 */

@Authorized()
@OpenAPI({ security: [{ ApiKeyAuth: [] }], summary: 'requirement controller' })
@JsonController('/requirements')
export class RequirementController extends GenericController<Requirement> {

    constructor(
        private requirementService: RequirementService
    ) { super(requirementService); }

    @Get()
    public find(@QueryParam('searchParam') searchParam: OrmQueryParam): Promise<Requirement[]> {
        return super.find(searchParam);
    }

    @Post('/search')
    public findBySearchObject(@QueryParam('searchParam') searchParam: OrmQueryParam, @Body() searchObject: OrmQueryBody): Promise<Requirement[]> {
        return super.findBySearchObject(searchParam, searchObject);
    }

    @Get('/:id')
    @OnUndefined(EntityNotFoundError)
    public one(@Param('id') id: number): Promise<Requirement | undefined> {
        return super.one(id);
    }

    @Post()
    public create(@CurrentUser({ required: true }) authUser: AuthenticatedUser, @Body() entity: Requirement): Promise<Requirement> {
        return super.create(authUser, entity);
    }

    @Put('/:id')
    @OnUndefined(EntityNotFoundError)
    public update(@CurrentUser({ required: true }) authUser: AuthenticatedUser, @Param('id') id: number,
                  @Body() entity: Requirement): Promise<Requirement | undefined> {
        return super.update(authUser, id, entity);
    }

    @Delete('/:id')
    public delete(@CurrentUser({required: true}) authUser: AuthenticatedUser, @Param('id') id: number): Promise<void> {
        return super.delete(authUser, id);
    }

    /** Relations */
    @Get('/:id/offer')
    public offer(@Param('id') id: number): Promise<Offer> {
        return this.requirementService.findOffer(id);
    }
    @Get('/:id/events')
    public requirementEvents(@Param('id') id: number, @QueryParam('searchParam') searchParam: OrmQueryParam): Promise<RequirementEvent[]> {
        return this.requirementService.findReqEventsByRequirement(id, OrmQueryParam.toORMQuery(searchParam));
    }

    @Get('/:id/user/:userId/events')
    public requirementEventsOfUser(@Param('id') id: number,
                                   @QueryParam('searchParam') searchParam: OrmQueryParam): Promise<RequirementEvent[]> {
        return this.requirementService.findReqEventsByRequirement(id, OrmQueryParam.toORMQuery(searchParam));
    }

}
