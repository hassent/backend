import {
    Authorized, Body, CurrentUser, Delete, Get, JsonController, OnUndefined, Param, Post, Put,
    QueryParam, Req
} from 'routing-controllers';
import { OpenAPI } from 'routing-controllers-openapi';

import { AuthenticatedUser } from '../../auth/AuthenticatedUser';
import { EntityNotFoundError } from '../errors/EntityNotFoundError';
import { OrmQueryBody } from '../models/Query/QueryBody';
import { OrmQueryParam } from '../models/Query/QueryParam';
import { Role } from '../models/Role/RoleModel';
import { User } from '../models/User/UserModel';
import { RoleService } from '../services/RoleService';
import { UserService } from '../services/UserService';

@Authorized()
@OpenAPI({security: [{ ApiKeyAuth: [] }], summary: 'role controller'})
@JsonController('/roles')
export class RoleController {

    constructor(
        private roleService: RoleService,
        private userService: UserService
    ) { }

    @Get()
    public find(@QueryParam('searchParam') searchParam: OrmQueryParam): Promise<Role[]> {
        return this.roleService.find(OrmQueryParam.toORMQuery(searchParam));
    }

    @Post('/search')
    public findBySearchObject(@QueryParam('searchParam') searchParam: OrmQueryParam, @Body() searchObject: OrmQueryBody): Promise<Role[]> {
        return this.roleService.find({...OrmQueryParam.toORMQuery(searchParam), ...OrmQueryBody.toORMQuery(searchObject)});
    }

    @Get('/:id')
    @OnUndefined(EntityNotFoundError)
    public one(@Param('id') id: number): Promise<Role | undefined> {
        return this.roleService.findOne(id);
    }

    @Post('/user/:userId')
    public async create(@CurrentUser({ required: true}) authUser: AuthenticatedUser, @Param('userId') userId: number, @Body() entity: Role): Promise<Role> {
        const user: User = await this.userService.findOne(userId);
        if (!user) {
            throw new EntityNotFoundError();
        }
        entity.user = Promise.resolve(user);
        return this.roleService.create(authUser, entity);
    }

    @Put('/:id')
    @OnUndefined(EntityNotFoundError)
    public update(@CurrentUser({required: true}) authUser: AuthenticatedUser, @Param('id') id: number, @Body() entity: Role): Promise<Role | undefined> {
        return this.roleService.update(authUser, id, entity);
    }

    @Delete('/:id')
    public delete(@CurrentUser({required: true}) authUser: AuthenticatedUser, @Param('id') id: number): Promise<void> {
        return this.roleService.delete(authUser, id);
    }

}
