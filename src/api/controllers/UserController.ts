import {
    Authorized, BadRequestError, Body, CurrentUser, Delete, Get, JsonController, OnUndefined, Param,
    Post, Put, QueryParam, Req
} from 'routing-controllers';
import { OpenAPI } from 'routing-controllers-openapi';

import { AuthenticatedUser } from '../../auth/AuthenticatedUser';
import { EntityNotFoundError } from '../errors/EntityNotFoundError';
import { Offer } from '../models/Offer/OfferModel';
import { OrmQueryBody } from '../models/Query/QueryBody';
import { OrmQueryParam } from '../models/Query/QueryParam';
import { RequirementEvent } from '../models/Requirement/RequirementEventModel';
import { Role } from '../models/Role/RoleModel';
import { User } from '../models/User/UserModel';
import { UserService } from '../services/UserService';
import { GenericController } from './GenericController';

@Authorized()
@OpenAPI({security: [{ ApiKeyAuth: [] }], summary: 'user controller'})
@JsonController('/users')
export class UserController extends GenericController<User> {

    constructor(
        private userService: UserService
    ) { super(userService); }

    @Get()
    public find(@QueryParam('searchParam') searchParam: OrmQueryParam): Promise<User[]> {
        return super.find(searchParam);
    }

    @Post('/search')
    public findBySearchObject(@QueryParam('searchParam') searchParam: OrmQueryParam, @Body() searchObject: OrmQueryBody): Promise<User[]> {
        return super.findBySearchObject(searchParam, searchObject);
    }

    @Get('/:id')
    @OnUndefined(EntityNotFoundError)
    public one(@Param('id') id: number): Promise<User | undefined> {
        return super.one(id);
    }

    @Post()
    public create(@CurrentUser({required: true}) authUser: AuthenticatedUser, @Body() entity: User): Promise<User> {
        return super.create(authUser, entity);
    }

    @Put('/:id')
    @OnUndefined(EntityNotFoundError)
    public update(@CurrentUser({required: true}) authUser: AuthenticatedUser, @Param('id') id: number, @Body() entity: User): Promise<User | undefined> {
        return super.update(authUser, id, entity);
    }

    @Delete('/:id')
    public delete(@CurrentUser({required: true}) authUser: AuthenticatedUser, @Param('id') id: number): Promise<void> {
        return super.delete(authUser, id);
    }

    /* Relations */
    @Authorized('view.self.role')
    @Get('/:id/roles')
    public roles(@Param('id') id: number): Promise<Role> {
        return this.userService.findRoles(id);
    }

    @Get('/:id/offers')
    public getUserOffers(@Param('id') id: number,
                         @QueryParam('searchParam') searchParam: OrmQueryParam): Promise<Offer[]> {
        return this.userService.findUserOffers(id, OrmQueryParam.toORMQuery(searchParam));
    }

    @Get('/:id/requirement/:requirementId/events')
    public getUserRequirementEvents(@Param('id') id: number,
                                    @Param('requirementId') requirementId: number,
                                    @QueryParam('searchParam') searchParam: OrmQueryParam): Promise<RequirementEvent[]> {
        return this.userService.findUserRequirementEventsOfRequirement(id, requirementId, OrmQueryParam.toORMQuery(searchParam));
    }

    @Post('/:id/requirement/:requirementId/events')
    @OnUndefined(EntityNotFoundError)
    public addRequirementEvents(@CurrentUser({ required: true}) authUser: AuthenticatedUser,
                                @Param('id') id: number,
                                @Param('requirementId') requirementId: number,
                                @Body() entity: RequirementEvent): Promise<RequirementEvent | undefined> {
        super.validateBody(entity)
        .catch(
            errors => {
                throw new BadRequestError(errors);
            });
        return this.userService.addRequirementEvents(id, requirementId, entity, authUser);
    }

}
