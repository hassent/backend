import { HttpError } from 'routing-controllers';

export class EntityNotFoundError extends HttpError {
    constructor() {
        super(404, 'Entity not found!');
    }
}
