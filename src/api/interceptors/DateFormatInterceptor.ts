import { Action, InterceptorInterface } from 'routing-controllers';

/*@Interceptor()*/
export class DateFormatInterceptor implements InterceptorInterface {

    public intercept(action: Action, content: any): any {
        return this.convertDates(content);
    }

    public convertDates(obj: any): any {
        if (!obj) {
            return obj;
        }
        for (const property in obj) {
            if (obj.hasOwnProperty(property)) {
                if (obj[property] instanceof Date) {
                    obj[property] = (obj[property] as Date).getTime();
                } else
                if (obj[property] instanceof Object) {
                    this.convertDates(obj[property]);
                } else
                if (obj[property] instanceof Array) {
                    for (const arrObj of obj[property]) {
                        this.convertDates(arrObj);
                    }
                }
            }
        }
        return obj;
    }

}
