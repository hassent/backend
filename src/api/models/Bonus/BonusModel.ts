import { Exclude } from 'class-transformer';
import { Column, Entity, ManyToOne } from 'typeorm';

import { GenericEntity } from '../GenericEntity/GenericEntity';
import { Reward } from '../Reward/RewardModel';

@Entity()
export class Bonus extends GenericEntity {

    @Exclude()
    public COMMON_ENTITY_DISCRIMINATOR = 'bonus';

    @Column()
    public amount: number;

    @ManyToOne(() => Reward, reward => reward.bonus)
    public reward: Promise<Reward>;

    public getCommonEntityDiscriminator(): string {
        return this.COMMON_ENTITY_DISCRIMINATOR;
    }

    public toString(): string {
        return `${this.genericToString()},
        `;
    }

}
