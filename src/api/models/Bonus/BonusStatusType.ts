/**
 *
 *
 * @export
 * @enum {number}
 */
export enum BonusStatusType {

    /**
     *
     */
    READY = 'READY',
    /**
     *
     */
    EXPIRED = 'EXPIRED',
    /**
     *
     */
    CONSUMED = 'CONSUMED',
    /**
     *
     */
    IN_USE = 'IN_USE',

}
