import { Exclude } from 'class-transformer';
import { Column, Entity } from 'typeorm';

import { GenericEntity } from '../GenericEntity/GenericEntity';
import { EventLogType } from './EventLogType';

@Entity()
export class EventLog extends GenericEntity {

    @Exclude()
    public COMMON_ENTITY_DISCRIMINATOR = 'entityLog';

    @Column({
        name: 'ip',
    })
    public ipAddress: string;

    @Column({
        name: 'entity_uid',
    })
    public entityUid: string;

    @Column({
        name: 'entity_type',
    })
    public entityType: string;

    @Column({
        type: 'enum',
        enum: EventLogType,
     })
    public logType: EventLogType;

    @Column('simple-json')
    public snapshot: {};

    public getCommonEntityDiscriminator(): string {
        return this.COMMON_ENTITY_DISCRIMINATOR;
    }

    public toString(): string {
        return `${this.genericToString()},
        ip: ${this.ipAddress},
        name: ${this.logType}`;
    }
}
