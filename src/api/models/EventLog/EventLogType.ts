/**
 * Entity log types
 *
 * @export
 * @enum {number}
 */
export enum EventLogType {

    /**
     * Entity created log type
     */
    ENTITY_CREATED = 'ENTITY_CREATED',
    /**
     * Entity updated log type
     */
    ENTITY_UPDATED = 'ENTITY_UPDATED',
    /**
     * Entity deleted log type
     */
    ENTITY_DELETED = 'ENTITY_DELETED',
}
