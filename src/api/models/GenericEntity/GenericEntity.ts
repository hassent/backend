import {
    Column, CreateDateColumn, Generated, PrimaryGeneratedColumn, UpdateDateColumn, VersionColumn
} from 'typeorm';

export abstract class GenericEntity {

    @PrimaryGeneratedColumn('increment')
    public id: number;

    @Column('uuid')
    @Generated('uuid')
    public uuid: string;

    @Column()
    @CreateDateColumn()
    public created: Date;

    @Column()
    @UpdateDateColumn()
    public updated: Date;

    @Column()
    @VersionColumn()
    public version: number;

    public abstract getCommonEntityDiscriminator(): string;

    public abstract toString(): string;

    public genericToString(): string {
        return `${this.getCommonEntityDiscriminator()}=>
        id: ${this.id},
        uuid: ${this.uuid},
        created: ${this.created},
        updated: ${this.updated},
        version: ${this.updated}`;
    }

}
