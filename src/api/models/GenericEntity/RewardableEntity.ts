import { Column } from 'typeorm';
import { IsNotEmpty } from 'class-validator';
import { GenericEntity } from './GenericEntity';

export abstract class RewardableEntity extends GenericEntity {

    @IsNotEmpty()
    @Column()
    public name: string;

    public toString(): string {
        return `name: ${this.name}`;
    }
}
