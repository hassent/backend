import { Exclude } from 'class-transformer';
import { IsNotEmpty, MinLength } from 'class-validator';
import { Column, Entity, OneToMany } from 'typeorm';

import { GenericEntity } from '../GenericEntity/GenericEntity';
import { Requirement } from '../Requirement/RequirementModel';
import { Reward } from '../Reward/RewardModel';
import { UserOffer } from './UserOfferModel';

@Entity()
export class Offer extends GenericEntity {

    @Exclude()
    public COMMON_ENTITY_DISCRIMINATOR = 'offer';

    @IsNotEmpty()
    @MinLength(5)
    @Column()
    public name: string;

    @Column()
    public validFrom: Date;

    @Column()
    public validTo: Date;

    @Column()
    public maxPerUser: number;

    @Column()
    public maxTotal: number;

    @OneToMany(type => Requirement, requirement => requirement.offer)
    public requirements: Promise<Requirement[]>;

    @OneToMany(type => Reward, reward => reward.offer)
    public rewards: Promise<Reward[]>;

    @OneToMany(type => UserOffer, userOffer => userOffer.offer)
    public userOffers: Promise<UserOffer[]>;

    public getCommonEntityDiscriminator(): string {
        return this.COMMON_ENTITY_DISCRIMINATOR;
    }

    public toString(): string {
        return `${this.genericToString()},
        name: ${this.name},
        validFrom: ${this.validFrom},
        validTo: ${this.validTo},
        maxPerUser: ${this.maxPerUser},
        maxTotal: ${this.maxTotal},
        `;
    }
}
