export enum OfferStatusType {
    /**
     *
     */
    REQUIREMENT_STARTED = 'REQUIREMENT_STARTED',
    /**
     *
     */
    REQUIREMENT_COMPLETED = 'REQUIREMENT_COMPLETED',
    /**
     *
     */
    REWARD_STARTED = 'REWARD_STARTED',
    /**
     *
     */
    REWARD_COMPLETED = 'REWARD_COMPLETED',
}
