import { Exclude } from 'class-transformer';
import { IsNotEmpty, MinLength } from 'class-validator';
import { Column, Entity, ManyToOne, OneToMany, OneToOne } from 'typeorm';

import { GenericEntity } from '../GenericEntity/GenericEntity';
import { RequirementEvent } from '../Requirement/RequirementEventModel';
import { RewardEvent } from '../Reward/RewardEventModel';
import { User } from '../User/UserModel';
import { Offer } from './OfferModel';
import { OfferStatusType } from './OfferStatusType';

@Entity()
export class UserOffer extends GenericEntity {

    @Exclude()
    public COMMON_ENTITY_DISCRIMINATOR = 'userOffer';

    @ManyToOne(type => Offer, offer => offer.userOffers)
    public offer: Promise<Offer>;

    @ManyToOne(type => User, user => user.userOffers)
    public user: Promise<User>;

    @OneToMany(type => RequirementEvent, requirementEvent => requirementEvent.userOffer)
    public requirementEvents: Promise<RequirementEvent[]>;

    @OneToMany(type => RewardEvent, rewardEvent => rewardEvent.userOffer)
    public rewardEvents: Promise<RewardEvent[]>;

    @IsNotEmpty()
    @Column({
        type: 'enum',
        enum: OfferStatusType,
     })
    public status: OfferStatusType;

    public getCommonEntityDiscriminator(): string {
        return this.COMMON_ENTITY_DISCRIMINATOR;
    }

    public toString(): string {
        return `${this.genericToString()},
        offer: ${this.offer.toString()},
        user: ${this.user.toString()},
        status: ${this.status},
        `;
    }
}
