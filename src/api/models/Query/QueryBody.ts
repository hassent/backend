import {
    Between, In, IsNull, LessThan, LessThanOrEqual, Like, MoreThan, MoreThanOrEqual, Not
} from 'typeorm';

import { TypeOrmOperator } from './TypeOrmOperator';

export class OrmQueryBody {

    public static toORMQuery(obj: OrmQueryBody): object {
        if (!obj) { return {}; }
        const result = { select: obj.select };
        const where = {};
        for (const item of obj.where) {
            switch (TypeOrmOperator[item.operator]) {
                case TypeOrmOperator.IN:
                    where[item.column] = In(item.value);
                    break;
                case TypeOrmOperator.NOTIN:
                    where[item.column] = Not(Like(item.value));
                    break;
                case TypeOrmOperator.LIKE:
                    where[item.column] = Like(item.value);
                    break;
                case TypeOrmOperator.NOTLIKE:
                    where[item.column] = Not(Like(item.value));
                    break;
                case TypeOrmOperator.NULL:
                    where[item.column] = IsNull();
                    break;
                case TypeOrmOperator.NOTNULL:
                    where[item.column] = Not(IsNull());
                    break;
                case TypeOrmOperator.MORETHAN:
                    where[item.column] = MoreThan(item.value);
                    break;
                case TypeOrmOperator.MORETHANOREQUAL:
                    where[item.column] = MoreThanOrEqual(item.value);
                    break;
                case TypeOrmOperator.LESSTHAN:
                    where[item.column] = LessThan(item.value);
                    break;
                case TypeOrmOperator.LESSTHANOREQUAL:
                    where[item.column] = LessThanOrEqual(item.value);
                    break;
                case TypeOrmOperator.BETWEEN:
                    where[item.column] = Between(item.value[this.accessors.from], item.value[this.accessors.to]);
                    break;
                default:
                    break;
            }
        }
        result[this.accessors.where] = where;
        return result;
    }
    private static accessors = { where: 'where', from: 'from', to: 'to'};

    private select: string[];

    private where: [{ column: string, value: any, operator: string }];

}
