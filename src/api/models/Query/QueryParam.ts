import { IsBoolean, IsNotEmpty, IsPositive } from 'class-validator';

export class OrmQueryParam {

    public static toORMQuery(obj: OrmQueryParam):
    {order: {[x: string]: 'ASC' | 'DESC'}, skip: number, take: number}  {
        if (!obj) { return {order: undefined, skip: undefined, take: undefined}; }
        return {
            order: {
                [obj.orderBy] : obj.asc ? 'ASC' : 'DESC',
            },
            skip: obj.skip,
            take: obj.take,
        };
    }

    @IsPositive()
    public take: number;

    @IsPositive()
    public skip: number;

    @IsNotEmpty()
    public orderBy: string;

    @IsBoolean()
    public asc: boolean;

}
