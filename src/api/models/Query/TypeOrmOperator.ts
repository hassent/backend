export enum TypeOrmOperator {
    BETWEEN,
    LESSTHAN,
    LESSTHANOREQUAL,
    MORETHAN,
    MORETHANOREQUAL,
    LIKE,
    NOTLIKE,
    NULL,
    NOTNULL,
    IN,
    NOTIN,
}
