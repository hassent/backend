import { Exclude } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';
import { Column, Entity, ManyToOne } from 'typeorm';

import { GenericEntity } from '../GenericEntity/GenericEntity';
import { UserOffer } from '../Offer/UserOfferModel';
import { User } from '../User/UserModel';
import { RequirementEventStatusType } from './RequirementEventStatusType';
import { Requirement } from './RequirementModel';

@Entity()
export class RequirementEvent extends GenericEntity {

    @Exclude()
    public COMMON_ENTITY_DISCRIMINATOR = 'requirementEvent';

    @IsNotEmpty()
    @Column({
        type: 'enum',
        enum: RequirementEventStatusType,
     })
    public status: RequirementEventStatusType;

    @ManyToOne(type => Requirement, requirement => requirement.requirementEvents)
    public requirement: Promise<Requirement>;

    @ManyToOne(type => UserOffer, userOffer => userOffer.rewardEvents)
    public userOffer: Promise<UserOffer>;

    public getCommonEntityDiscriminator(): string {
        return this.COMMON_ENTITY_DISCRIMINATOR;
    }

    public toString(): string {
        return `${this.genericToString()},
        `;
    }

}
