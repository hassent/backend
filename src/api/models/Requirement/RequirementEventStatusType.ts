export enum RequirementEventStatusType {

    /**
     *
     */
    AD_LOADED = 'AD_LOADED',
    /**
     *
     */
    AD_FIRST_QUARTILE = 'AD_FIRST_QUARTILE',
    /**
     *
     */
    AD_SECOND_QUARTILE = 'AD_SECOND_QUARTILE',
    /**
     *
     */
    AD_COMPLETED = 'AD_COMPLETED',
    /**
     *
     */
    AD_RATED = 'AD_RATED',
    /**
     *
     */
    AD_MUTED = 'AD_MUTED',
    /**
     *
     */
    SURVEY_LOADED = 'SURVEY_LOADED',
    /**
     *
     */
    SURVEY_COMPLETED = 'SURVEY_COMPLETED',
}
