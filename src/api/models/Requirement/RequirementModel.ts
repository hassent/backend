import { Exclude } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';
import { Entity, ManyToOne, OneToMany } from 'typeorm';

import { GenericEntity } from '../GenericEntity/GenericEntity';
import { Offer } from '../Offer/OfferModel';
import { RequirementEvent } from './RequirementEventModel';

@Entity()
export class Requirement extends GenericEntity {

    @Exclude()
    public COMMON_ENTITY_DISCRIMINATOR = 'requirement';

    @IsNotEmpty()
    @ManyToOne(type => Offer, offer => offer.requirements)
    public offer: Promise<Offer>;

    @OneToMany(type => RequirementEvent, requirementEvent => requirementEvent.requirement)
    public requirementEvents: Promise<RequirementEvent[]>;

    public getCommonEntityDiscriminator(): string {
        return this.COMMON_ENTITY_DISCRIMINATOR;
    }

    public toString(): string {
        return `${this.genericToString()},
        `;
    }

}
