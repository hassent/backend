import { Exclude } from 'class-transformer';
import { Column, Entity, ManyToOne } from 'typeorm';

import { GenericEntity } from '../GenericEntity/GenericEntity';
import { UserOffer } from '../Offer/UserOfferModel';
import { Reward } from './RewardModel';
import { RewardStatusType } from './RewardStatusType';

@Entity()
export class RewardEvent extends GenericEntity {

    @Exclude()
    public COMMON_ENTITY_DISCRIMINATOR = 'rewardEvent';

    @Column({
        type: 'enum',
        enum: RewardStatusType,
     })
    public status: RewardStatusType;

    @ManyToOne(type => Reward, reward => reward.rewardEvents)
    public reward: Promise<Reward>;

    @ManyToOne(type => UserOffer, userOffer => userOffer.rewardEvents)
    public userOffer: Promise<UserOffer>;

    public getCommonEntityDiscriminator(): string {
        return this.COMMON_ENTITY_DISCRIMINATOR;
    }

    public toString(): string {
        return `${this.genericToString()},
        `;
    }

}
