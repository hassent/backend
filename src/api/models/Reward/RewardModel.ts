import { Exclude } from 'class-transformer';
import { IsNotEmpty, IsNumber, Min } from 'class-validator';
import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';

import { Bonus } from '../Bonus/BonusModel';
import { GenericEntity } from '../GenericEntity/GenericEntity';
import { Offer } from '../Offer/OfferModel';
import { RewardEvent } from './RewardEventModel';
import { RewardType } from './RewardType';

@Entity()
export class Reward extends GenericEntity {

    @Exclude()
    public COMMON_ENTITY_DISCRIMINATOR = 'reward';

    @Min(1)
    @IsNumber()
    @Column()
    public amount: number;

    @IsNotEmpty()
    @Column({
        type: 'enum',
        enum: RewardType,
     })
    public type: RewardType;

    @ManyToOne(type => Offer, offer => offer.rewards)
    public offer: Promise<Offer>;

    @OneToMany(type => RewardEvent, rewardEvents => rewardEvents.reward)
    public rewardEvents: Promise<RewardEvent[]>;

    @OneToMany(type => Bonus, bonus => bonus.reward)
    public bonus: Promise<Bonus[]>;

    public getCommonEntityDiscriminator(): string {
        return this.COMMON_ENTITY_DISCRIMINATOR;
    }

    public toString(): string {
        return `${this.genericToString()},
        amount: ${this.amount}
        `;
    }

}
