import { Exclude } from 'class-transformer';
import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

import { RewardableEntity } from '../GenericEntity/RewardableEntity';
import { User } from '../User/UserModel';

@Entity()
export class Role extends RewardableEntity {

    @Exclude()
    public COMMON_ENTITY_DISCRIMINATOR = 'role';

    @Column('simple-json')
    public role: {
        self: {},
        all: {},
        several: {}
    };

    @OneToOne(type => User, user => user.token)
    @JoinColumn()
    public user: Promise<User>;

    public getCommonEntityDiscriminator(): string {
        return this.COMMON_ENTITY_DISCRIMINATOR;
    }
    public toString(): string {
        return `${this.genericToString()},
        ${super.toString()}`;
    }

}
