import { User } from '../User/UserModel';
import { Role } from './RoleModel';

export class RoleUtil {
    public static hasRole(user: User, role: Role, entityId: number, scope: string): Promise<boolean> {
            const requestedScope = RoleUtil.requestedScope(scope);
            return Promise.resolve(true);
    }

    public static requestedScope(scope: string): {scope: string, entityName: string} {
        const scopeArr: string[] = scope.split('.');
        return {
            scope: scopeArr[1],
            entityName: scopeArr[2],
        };
    }
}
