import { Exclude } from 'class-transformer';
import { Entity } from 'typeorm';

import { RewardableEntity } from '../GenericEntity/RewardableEntity';

@Entity()
export class Survey extends RewardableEntity {

    @Exclude()
    public COMMON_ENTITY_DISCRIMINATOR = 'survey';

    public getCommonEntityDiscriminator(): string {
        return this.COMMON_ENTITY_DISCRIMINATOR;
    }
    public toString(): string {
        return `${this.genericToString()},
        ${super.toString()}`;
    }
}
