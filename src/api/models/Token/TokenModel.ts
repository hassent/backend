import * as bcrypt from 'bcrypt';
import { Exclude } from 'class-transformer';
import { BeforeInsert, BeforeUpdate, Column, Entity, JoinColumn, OneToOne } from 'typeorm';

import { GenericEntity } from '../GenericEntity/GenericEntity';
import { User } from '../User/UserModel';

@Entity()
export class Token extends GenericEntity {

    public static TOKEN_LENGTH = 30;
    public static TOKEN_SEED = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

    @Exclude()
    public static COMMON_ENTITY_DISCRIMINATOR = 'token';

    public static hashToken(token: string): Promise<string> {
        return new Promise((resolve, reject) => {
            bcrypt.hash(token, 10, (err, hash) => {
                if (err) {
                    return reject(err);
                }
                resolve(hash);
            });
        });
    }

    public static compareToken(token: Token, plainToken: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            bcrypt.compare(plainToken, token.token, (err, res) => {
                resolve(res === true);
            });
        });
    }

    public static generateAuthToken(): string {
        let token = '';
        do {
            token += [...Array(this.TOKEN_LENGTH)]
            // tslint:disable-next-line:no-bitwise
            .map(i => this.TOKEN_SEED[Math.random() * this.TOKEN_SEED.length | 0]).join('');
        } while (token.length < this.TOKEN_LENGTH);
        return token.substr(0, this.TOKEN_LENGTH);
   }

    @Column()
    public token: string;

    @OneToOne(type => User, user => user.token)
    @JoinColumn()
    public user: Promise<User>;

    constructor() {
        super();
        this.token = Token.generateAuthToken();
    }

    @BeforeInsert()
    public async hashTokenOnInsert(): Promise<void> {
        this.token = await Token.hashToken(this.token);
    }

    @BeforeUpdate()
    public async hashTokenOnUpdate(): Promise<void> {
        this.token = await Token.hashToken(this.token);
    }

    public getCommonEntityDiscriminator(): string {
        return Token.COMMON_ENTITY_DISCRIMINATOR;
    }

    public toString(): string {
        return `${this.genericToString()},
        token: ${this.token}`;
    }
}
