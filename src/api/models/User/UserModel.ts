import * as bcrypt from 'bcrypt';
import { Exclude } from 'class-transformer';
import { IsEmail, IsNotEmpty } from 'class-validator';
import { Request } from 'express';
import { BeforeInsert, Column, Entity, OneToMany, OneToOne, Unique } from 'typeorm';

import { AuthenticatedUser } from '../../../auth/AuthenticatedUser';
import { env } from '../../../env';
import { GenericEntity } from '../GenericEntity/GenericEntity';
import { UserOffer } from '../Offer/UserOfferModel';
import { RequirementEvent } from '../Requirement/RequirementEventModel';
import { RewardEvent } from '../Reward/RewardEventModel';
import { Role } from '../Role/RoleModel';
import { Token } from '../Token/TokenModel';

@Entity()
@Unique(['username'])
export class User extends GenericEntity {

    public static hashPassword(password: string): Promise<string> {
        return new Promise((resolve, reject) => {
            bcrypt.hash(password, 10, (err, hash) => {
                if (err) {
                    return reject(err);
                }
                resolve(hash);
            });
        });
    }

    public static comparePassword(user: User, password: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, user.password, (err, res) => {
                resolve(res === true);
            });
        });
    }

    @Exclude()
    public COMMON_ENTITY_DISCRIMINATOR = 'user';

    @IsNotEmpty()
    @Column({ name: 'first_name' })
    public firstName: string;

    @IsNotEmpty()
    @Column({ name: 'last_name' })
    public lastName: string;

    @IsNotEmpty()
    @IsEmail()
    @Column()
    public email: string;

    @IsNotEmpty()
    @Column()
    @Exclude()
    public password: string;

    @IsNotEmpty()
    @Column()
    public username: string;

    @OneToOne(type => Token, token => token.user)
    public token: Promise<Token>;

    @OneToMany(type => UserOffer, userOffer => userOffer.user)
    public userOffers: Promise<UserOffer[]>;

    @OneToOne(type => Role, role => role.user)
    public role: Promise<Role>;

    constructor(firstName: string, lastName: string, email: string, username: string) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = env.dummyUser.secret;
    }

    @BeforeInsert()
    public async hashPassword(): Promise<void> {
        this.password = await User.hashPassword(this.password);
    }

    public toAuthenticatedUser(req: Request, token: string): AuthenticatedUser {
        return new AuthenticatedUser(this.id, this.email, req.ip, token);
    }

    public getCommonEntityDiscriminator(): string {
        return this.COMMON_ENTITY_DISCRIMINATOR;
    }

    public toString(): string {
        return `${this.genericToString()},
        firstName: ${this.firstName},
        lastName: ${this.lastName},
        email: ${this.email}
        `;
    }

}
