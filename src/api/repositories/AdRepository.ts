import { EntityRepository, Repository } from 'typeorm';

import { Ad } from '../models/Ad/AdModel';

@EntityRepository(Ad)
export class AdRepository extends Repository<Ad> { }
