import { EntityRepository, Repository } from 'typeorm';

import { Bonus } from '../models/Bonus/BonusModel';

@EntityRepository(Bonus)
export class BonusRepository extends Repository<Bonus> { }
