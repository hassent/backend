import { EntityRepository, Repository } from 'typeorm';

import { EventLog } from '../models/EventLog/EventLogModel';

@EntityRepository(EventLog)
export class EventLogRepository extends Repository<EventLog> { }
