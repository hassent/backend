import { EntityRepository, Repository } from 'typeorm';

import { Offer } from '../models/Offer/OfferModel';

@EntityRepository(Offer)
export class OfferRepository extends Repository<Offer> { }
