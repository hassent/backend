import { EntityRepository, Repository } from 'typeorm';

import { RequirementEvent } from '../models/Requirement/RequirementEventModel';

@EntityRepository(RequirementEvent)
export class RequirementEventRepository extends Repository<RequirementEvent> { }
