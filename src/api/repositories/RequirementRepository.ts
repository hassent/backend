import { EntityRepository, Repository } from 'typeorm';

import { Requirement } from '../models/Requirement/RequirementModel';

@EntityRepository(Requirement)
export class RequirementRepository extends Repository<Requirement> { }
