import { EntityRepository, Repository } from 'typeorm';

import { RewardEvent } from '../models/Reward/RewardEventModel';

@EntityRepository(RewardEvent)
export class RewardEventRepository extends Repository<RewardEvent> { }
