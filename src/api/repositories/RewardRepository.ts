import { EntityRepository, Repository } from 'typeorm';

import { Reward } from '../models/Reward/RewardModel';

@EntityRepository(Reward)
export class RewardRepository extends Repository<Reward> { }
