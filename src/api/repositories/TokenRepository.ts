import { EntityRepository, Repository } from 'typeorm';

import { Token } from '../models/Token/TokenModel';

@EntityRepository(Token)
export class TokenRepository extends Repository<Token>  {

}
