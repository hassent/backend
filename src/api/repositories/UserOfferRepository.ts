import { EntityRepository, Repository } from 'typeorm';

import { UserOffer } from '../models/Offer/UserOfferModel';

@EntityRepository(UserOffer)
export class UserOfferRepository extends Repository<UserOffer> { }
