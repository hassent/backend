import { EntityRepository, Repository } from 'typeorm';

import { User } from '../models/User/UserModel';

@EntityRepository(User)
export class UserRepository extends Repository<User>  {

}
