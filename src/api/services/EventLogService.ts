import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { EventLog } from '../models/EventLog/EventLogModel';
import { EventLogRepository } from '../repositories/EventLogRepository';
import { GenericEntityService } from './GenericEntityService';

@Service()
export class EventLogService extends GenericEntityService<EventLog> {

    constructor(
        @OrmRepository() private _eventLogRepository: EventLogRepository
        ) {  super(_eventLogRepository); }

        public getRepository(): EventLogRepository {
            return this._eventLogRepository;
        }

}
