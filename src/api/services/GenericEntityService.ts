import { detailedDiff } from 'deep-object-diff';
import { Service } from 'typedi';
import {
    FindManyOptions, FindOneOptions, getManager, Repository, SelectQueryBuilder
} from 'typeorm';

import { AuthenticatedUser } from '../../auth/AuthenticatedUser';
import { EventDispatcher, EventDispatcherInterface } from '../../decorators/EventDispatcher';
import { GenericEntity } from '../models/GenericEntity/GenericEntity';
import { events } from '../subscribers/events';

@Service()
export class GenericEntityService<T extends GenericEntity> {

    @EventDispatcher() private eventDispatcher: EventDispatcherInterface;

    constructor(private repo: Repository<T>) { }

    /** searches */
    public find(query: FindManyOptions): Promise<T[]> {
        return this.repo.find(query);
    }

    public findAndCount(query: FindManyOptions): Promise<[T[], number]> {
        return this.repo.findAndCount(query);
    }

    public findByIds(ids: number[]): Promise<T[]> {
        return this.repo.findByIds(ids);
    }

    public findOne(id: number): Promise<T | undefined> {
        return this.repo.findOne({ where: { id } });
    }

    public findOneByOption(query: FindOneOptions<T>): Promise<T | undefined> {
        return this.repo.findOne( query );
    }

    public findOneOrFail(query: FindOneOptions<T>): Promise<T | undefined> {
        return this.repo.findOneOrFail(query);
    }

    public async lazyLoaded(id: number, relatedEntity: string): Promise<GenericEntity[] | GenericEntity | undefined> {
        const entity = await this.repo.findOne({where: { id }, relations: [relatedEntity] });
        return entity[relatedEntity];
    }

    public async getQueryBuilder(alias: string): Promise<SelectQueryBuilder<T>> {
        return this.repo.createQueryBuilder(alias);
    }

    /** CUD */
    public async create(authUser: AuthenticatedUser, entity: T): Promise<T> {
        const newEntity = await this.repo.save(entity as any);
        const data = {
            authUser,
            content: newEntity,
        };
        const evt = events[newEntity.getCommonEntityDiscriminator()] || events.entity;
        this.eventDispatcher.dispatch(evt.created, data);
        return newEntity;
    }

    public async update(authUser: AuthenticatedUser, id: number, entity: T): Promise<T | undefined> {
        const oldEntity = await this.repo.findOne(id);
        if (!oldEntity) {
            return Promise.resolve(undefined);
        }

        const updatedEntity = await this.repo.save(Object.assign(oldEntity, entity) as any);

        const data = {
            authUser,
            content: {
                entity: oldEntity,
                delta: detailedDiff(oldEntity, entity),
            },
        };
        const evt = events[updatedEntity.getCommonEntityDiscriminator()] || events.entity;
        this.eventDispatcher.dispatch(evt.updated, data);
        return updatedEntity;
    }

    public async delete(authUser: AuthenticatedUser, id: number): Promise<void> {
        const entity = await this.repo.findOne(id);
        await this.repo.delete(id);

        const data = {
            authUser,
            content: entity,
        };
        const evt = events[entity.getCommonEntityDiscriminator()] || events.entity;
        this.eventDispatcher.dispatch(evt.deleted, data);
        return;
    }
}
