import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { Offer } from '../models/Offer/OfferModel';
import { Requirement } from '../models/Requirement/RequirementModel';
import { Reward } from '../models/Reward/RewardModel';
import { OfferRepository } from '../repositories/OfferRepository';
import { GenericEntityService } from './GenericEntityService';

@Service()
export class OfferService extends GenericEntityService<Offer> {

    constructor(
        @OrmRepository() private offerRepository: OfferRepository
    ) {  super(offerRepository); }

    /** Relations */
    public async findRequirements(id: number): Promise<Requirement[]> {
        return this.findOne(id).then( offer => offer.requirements).catch( e => Promise.reject(undefined));
    }

    public async findRewards(id: number): Promise<Reward[]> {
        return this.findOne(id).then( offer => offer.rewards).catch( e => Promise.reject(undefined));
    }

}
