import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { RequirementEvent } from '../models/Requirement/RequirementEventModel';
import { Requirement } from '../models/Requirement/RequirementModel';
import { RequirementEventRepository } from '../repositories/RequirementEventRepository';
import { GenericEntityService } from './GenericEntityService';

@Service()
export class RequirementEventService extends GenericEntityService<RequirementEvent> {

    constructor(
        @OrmRepository() private requirementEventRepository: RequirementEventRepository
    ) {  super(requirementEventRepository); }

    /** Relations */
    public async findRequirement(id: number): Promise<Requirement> {
        return this.findOne(id).then(requirementEvent => requirementEvent.requirement).catch(e => Promise.reject(undefined));
    }

}
