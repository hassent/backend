import { Container, Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { Offer } from '../models/Offer/OfferModel';
import { RequirementEvent } from '../models/Requirement/RequirementEventModel';
import { Requirement } from '../models/Requirement/RequirementModel';
import { RequirementRepository } from '../repositories/RequirementRepository';
import { GenericEntityService } from './GenericEntityService';
import { RequirementEventService } from './RequirementEventService';

@Service()
export class RequirementService extends GenericEntityService<Requirement> {

    private requirementEventService = Container.get<RequirementEventService>(RequirementEventService);

    constructor(
        @OrmRepository() private requirementRepository: RequirementRepository
    ) {  super(requirementRepository); }

    /** Relations */
    public async findOffer(id: number): Promise<Offer> {
        return this.findOne(id).then(requirement => requirement.offer).catch(e => Promise.reject(undefined));
    }

    public async findReqEventsByUserAndRequirement(userId: number, requirementId: number, searchParam?: object): Promise<RequirementEvent[]> {
        return this.requirementEventService.find({...searchParam, ...{ where: { userId, requirementId } } });
    }

    public async findReqEventsByUser(userId: number, searchParam?: object): Promise<RequirementEvent[]> {
        return this.requirementEventService.find({...searchParam, ...{ where: { userId } } });
    }

    public async findReqEventsByRequirement(requirementId: number, searchParam?: object): Promise<RequirementEvent[]> {
        return this.requirementEventService.find({...searchParam, ...{ where: { requirementId } } });
    }

}
