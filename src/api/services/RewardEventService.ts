import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { RewardEvent } from '../models/Reward/RewardEventModel';
import { Reward } from '../models/Reward/RewardModel';
import { RewardEventRepository } from '../repositories/RewardEventRepository';
import { GenericEntityService } from './GenericEntityService';

@Service()
export class RewardEventService extends GenericEntityService<RewardEvent> {

    constructor(
        @OrmRepository() private rewardEventRepository: RewardEventRepository
    ) {  super(rewardEventRepository); }

    /** Relations */
    public async findReward(id: number): Promise<Reward> {
        return this.findOne(id).then(rewardEvent => rewardEvent.reward).catch(e => Promise.reject(undefined));
    }

}
