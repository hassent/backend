import { Container, Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { Offer } from '../models/Offer/OfferModel';
import { RewardEvent } from '../models/Reward/RewardEventModel';
import { Reward } from '../models/Reward/RewardModel';
import { RewardRepository } from '../repositories/RewardRepository';
import { GenericEntityService } from './GenericEntityService';
import { RewardEventService } from './RewardEventService';

@Service()
export class RewardService extends GenericEntityService<Reward> {

    private rewardEventService = Container.get<RewardEventService>(RewardEventService);

    constructor(
        @OrmRepository() private rewardRepository: RewardRepository
    ) {  super(rewardRepository); }

    /** Relations */
    public async findOffer(id: number): Promise<Offer> {
        return this.findOne(id).then(reward => reward.offer).catch(e => Promise.reject(undefined));
    }

    public async findRewEventsByUserAndReward(userId: number, rewardId: number, searchParam?: object): Promise<RewardEvent[]> {
        return this.rewardEventService.find({...searchParam, ...{ where: { userId, rewardId } } });
    }

    public async findRewEventsByUser(userId: number, searchParam?: object): Promise<RewardEvent[]> {
        return this.rewardEventService.find({...searchParam, ...{ where: { userId } } });
    }

    public async findRewEventsByReward(rewardId: number, searchParam?: object): Promise<RewardEvent[]> {
        return this.rewardEventService.find({...searchParam, ...{ where: { rewardId } } });
    }

}
