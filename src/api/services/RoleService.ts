import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { Role } from '../models/Role/RoleModel';
import { RoleRepository } from '../repositories/RoleRepository';
import { GenericEntityService } from './GenericEntityService';

@Service()
export class RoleService extends GenericEntityService<Role> {

    constructor(
        @OrmRepository() private _roleRepository: RoleRepository
    ) {
        super( _roleRepository );
     }
}
