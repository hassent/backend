import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { AuthenticatedUser } from '../../auth/AuthenticatedUser';
import { Token } from '../models/Token/TokenModel';
import { User } from '../models/User/UserModel';
import { TokenRepository } from '../repositories/TokenRepository';
import { GenericEntityService } from './GenericEntityService';

@Service()
export class TokenService extends GenericEntityService<Token> {

    constructor(
        @OrmRepository() private _tokenRepository: TokenRepository
    ) {
        super( _tokenRepository );
     }

     public async refreshAndGetTokenForUser(user: User): Promise<string> {
        const plainToken: string = Token.generateAuthToken();
        let tokenObject: Token = await user.token;

        if (tokenObject) {
            tokenObject.token = plainToken;
            await this.update(new AuthenticatedUser(), tokenObject.id, tokenObject);
        } else {
            tokenObject =  new Token();
            tokenObject.user = Promise.resolve(user);
            tokenObject.token = plainToken;
            await this.create(new AuthenticatedUser(), tokenObject);
        }

        return plainToken;

     }

 }
