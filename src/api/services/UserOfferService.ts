import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { Offer } from '../models/Offer/OfferModel';
import { UserOffer } from '../models/Offer/UserOfferModel';
import { Requirement } from '../models/Requirement/RequirementModel';
import { Reward } from '../models/Reward/RewardModel';
import { OfferRepository } from '../repositories/OfferRepository';
import { UserOfferRepository } from '../repositories/UserOfferRepository';
import { GenericEntityService } from './GenericEntityService';

@Service()
export class UserOfferService extends GenericEntityService<UserOffer> {

    constructor(
        @OrmRepository() private userOfferRepository: UserOfferRepository
    ) {  super(userOfferRepository); }

    /** Relations */
    public async findOffer(id: number): Promise<Offer> {
        return this.findOne(id).then(userOffer => userOffer.offer).catch(e => Promise.reject(undefined));
    }
}
