import { Container, Service } from 'typedi';
import { SelectQueryBuilder } from 'typeorm';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { AuthenticatedUser } from '../../auth/AuthenticatedUser';
import { Offer } from '../models/Offer/OfferModel';
import { UserOffer } from '../models/Offer/UserOfferModel';
import { RequirementEvent } from '../models/Requirement/RequirementEventModel';
import { Requirement } from '../models/Requirement/RequirementModel';
import { RewardEvent } from '../models/Reward/RewardEventModel';
import { Reward } from '../models/Reward/RewardModel';
import { Role } from '../models/Role/RoleModel';
import { User } from '../models/User/UserModel';
import { UserRepository } from '../repositories/UserRepository';
import { GenericEntityService } from './GenericEntityService';
import { OfferService } from './OfferService';
import { RequirementEventService } from './RequirementEventService';
import { RequirementService } from './RequirementService';
import { RewardEventService } from './RewardEventService';
import { RewardService } from './RewardService';
import { UserOfferService } from './UserOfferService';

@Service()
export class UserService extends GenericEntityService<User> {

    private offerService = Container.get<OfferService>(OfferService);
    private userOfferService = Container.get<UserOfferService>(UserOfferService);
    private requirementService = Container.get<RequirementService>(RequirementService);
    private requirementEventService = Container.get<RequirementEventService>(RequirementEventService);
    private rewardService = Container.get<RewardService>(RewardService);
    private rewardEventService = Container.get<RewardEventService>(RewardEventService);

    constructor(
        @OrmRepository() private _userRepository: UserRepository
    ) {
        super(_userRepository);
    }

    /** Relations */
    public async findRoles(id: number): Promise<Role> {
        return this.findOne(id).then(user => user.role).catch(e => Promise.reject(undefined));
    }

    public async findUserOffers(userId: number, searchParam: { skip: number, take: number }): Promise<Offer[]> {
        const queryBuilder: SelectQueryBuilder<Offer> = await this.offerService.getQueryBuilder('offer');
        return queryBuilder
            .innerJoin('requirement', 'req', 'req.offerId = offer.id')
            .innerJoin('requirement_event', 'reqEve', 'reqEve.requirementId = req.id and reqEve.userId = :userId', { userId })
            .skip(searchParam.skip || 0)
            .take(searchParam.take)
            .getMany();
    }

    public async findUserRequirementEventsOfRequirement(userId: number, requirementId: number, searchParam: object): Promise<RequirementEvent[]> {
        return this.requirementService.findReqEventsByUserAndRequirement(userId, requirementId, searchParam);
    }

    public async findAllUserRequirementEvents(userId: number, searchParam: object): Promise<RequirementEvent[]> {
        return this.requirementService.findReqEventsByUser(userId, searchParam);
    }

    public async addRequirementEvents(userOfferId: number, requirementId: number,
                                      entity: RequirementEvent, authUser: AuthenticatedUser): Promise<RequirementEvent | undefined> {
        const userOffer: UserOffer = await this.userOfferService.findOne(userOfferId);
        if (!userOffer) { return undefined; }
        const requirement: Requirement = await this.requirementService.findOne(requirementId);
        if (!requirement) { return undefined; }
        entity.userOffer = Promise.resolve(userOffer);
        entity.requirement = Promise.resolve(requirement);
        return this.requirementEventService.create(authUser, entity);
    }

    public async addRewardEvents(userOfferId: number, rewardId: number,
                                 entity: RewardEvent, authUser: AuthenticatedUser): Promise<RewardEvent | undefined> {
        const userOffer: UserOffer = await this.userOfferService.findOne(userOfferId);
        if (!userOffer) { return undefined; }
        const reward: Reward = await this.rewardService.findOne(rewardId);
        if (!reward) { return undefined; }
        entity.userOffer = Promise.resolve(userOffer);
        entity.reward = Promise.resolve(reward);
        return this.rewardEventService.create(authUser, entity);
    }
}
