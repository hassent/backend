import { EventSubscriber, On } from 'event-dispatch';
import { Container } from 'typedi';

import { AuthenticatedUser } from '../../auth/AuthenticatedUser';
import { EventLog } from '../models/EventLog/EventLogModel';
import { EventLogType } from '../models/EventLog/EventLogType';
import { GenericEntity } from '../models/GenericEntity/GenericEntity';
import { EventLogService } from '../services/EventLogService';
import { events } from './events';

@EventSubscriber()
export class EntityEventSubscriber {

    private eventLogService: EventLogService = Container.get<EventLogService>(EventLogService);

    @On([
        events.user.created,
        events.offer.created,
    ])
    public async onEntityCreate(data: any): Promise<void> {
        const eLog = this.constructEventLog(data, EventLogType.ENTITY_CREATED);
        await this.eventLogService.create(eLog.authUser, eLog.log);
    }

    @On([
        events.user.updated,
        events.offer.updated,
    ])
    public async onEntityUpdate(data: any): Promise<void> {
        const eLog = this.constructEventLog(data, EventLogType.ENTITY_UPDATED);
        await this.eventLogService.create(eLog.authUser, eLog.log);
    }

    @On([
        events.user.deleted,
        events.offer.deleted,
    ])
    public async onEntityDelete(data: any): Promise<void> {
        const eLog = this.constructEventLog(data, EventLogType.ENTITY_DELETED);
        await this.eventLogService.create(eLog.authUser, eLog.log);
    }

    public constructEventLog(data: any, eventLogType: EventLogType): { log: EventLog, authUser: AuthenticatedUser } {
        const newEventLog: EventLog = new EventLog();
        const entity = data.content as GenericEntity;
        const authUser: AuthenticatedUser = data.authUser as AuthenticatedUser;
        newEventLog.ipAddress = authUser.currentIpAddress;
        newEventLog.entityUid = entity.uuid;
        newEventLog.entityType = entity.getCommonEntityDiscriminator();
        switch (eventLogType) {
            case EventLogType.ENTITY_CREATED:
                newEventLog.logType = EventLogType.ENTITY_CREATED;
                newEventLog.snapshot = entity;
                break;
            case EventLogType.ENTITY_UPDATED:
                newEventLog.logType = EventLogType.ENTITY_UPDATED;
                newEventLog.snapshot = data.content.delta;
                break;
            case EventLogType.ENTITY_DELETED:
                newEventLog.logType = EventLogType.ENTITY_DELETED;
                newEventLog.snapshot = entity;
                break;
            default:
                break;
        }
        return { log: newEventLog, authUser };
    }

}
