import { EventSubscriber, On } from 'event-dispatch';
import { Container } from 'typedi';
import { In } from 'typeorm';

import { AuthenticatedUser } from '../../auth/AuthenticatedUser';
import { OfferStatusType } from '../models/Offer/OfferStatusType';
import { RequirementEvent } from '../models/Requirement/RequirementEventModel';
import { RequirementEventStatusType } from '../models/Requirement/RequirementEventStatusType';
import { EventLogService } from '../services/EventLogService';
import { OfferService } from '../services/OfferService';
import { RequirementEventService } from '../services/RequirementEventService';
import { UserOfferService } from '../services/UserOfferService';
import { events } from './events';

@EventSubscriber()
export class RequirementEventSubscriber {

    private eventLogService: EventLogService = Container.get<EventLogService>(EventLogService);
    private requirementEventService: RequirementEventService = Container.get<RequirementEventService>(RequirementEventService);
    private offerService: OfferService = Container.get<OfferService>(OfferService);
    private userOfferService: UserOfferService = Container.get<UserOfferService>(UserOfferService);

    @On([
        events.requirementEvent.created,
    ])
    public async onRequirementEventCreated(data: any): Promise<void> {
        const authUser: AuthenticatedUser = data.authUser as AuthenticatedUser;
        const entity = data.content as RequirementEvent;
        switch (entity.status) {
            case RequirementEventStatusType.AD_COMPLETED:
            case RequirementEventStatusType.SURVEY_COMPLETED:
                    const userOffer = await entity.userOffer;
                    const offer = await userOffer.offer;
                    const offerRequirements = await offer.requirements;
                    const requirementEvents = await this.requirementEventService.find(
                        {
                            where:
                            {
                                userOfferId: userOffer.id,
                                status: In([
                                    RequirementEventStatusType.AD_COMPLETED,
                                    RequirementEventStatusType.SURVEY_COMPLETED,
                                ]),
                            },
                        });
                        for (const requirementEvent of requirementEvents) {
                            const requirementFromReqEvent = await requirementEvent.requirement;
                            delete offerRequirements[offerRequirements.indexOf(requirementFromReqEvent)];
                        }
                        if (offerRequirements.filter(Boolean).length === 0) {
                            // requirement fulfilled
                            userOffer.status = OfferStatusType.REQUIREMENT_COMPLETED;
                            this.userOfferService.update(authUser, userOffer.id, userOffer);
                        }
                    break;
            default:
                break;
        }
    }
}
