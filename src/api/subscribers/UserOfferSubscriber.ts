import { EventSubscriber, On } from 'event-dispatch';
import { Container } from 'typedi';
import { In } from 'typeorm';

import { AuthenticatedUser } from '../../auth/AuthenticatedUser';
import { EventLog } from '../models/EventLog/EventLogModel';
import { EventLogType } from '../models/EventLog/EventLogType';
import { GenericEntity } from '../models/GenericEntity/GenericEntity';
import { Offer } from '../models/Offer/OfferModel';
import { OfferStatusType } from '../models/Offer/OfferStatusType';
import { UserOffer } from '../models/Offer/UserOfferModel';
import { RequirementEvent } from '../models/Requirement/RequirementEventModel';
import { RequirementEventStatusType } from '../models/Requirement/RequirementEventStatusType';
import { Requirement } from '../models/Requirement/RequirementModel';
import { RewardStatusType } from '../models/Reward/RewardStatusType';
import { EventLogService } from '../services/EventLogService';
import { OfferService } from '../services/OfferService';
import { RequirementEventService } from '../services/RequirementEventService';
import { UserOfferService } from '../services/UserOfferService';
import { events } from './events';

@EventSubscriber()
export class UserOfferSubscriber {

    private eventLogService: EventLogService = Container.get<EventLogService>(EventLogService);
    private requirementEventService: RequirementEventService = Container.get<RequirementEventService>(RequirementEventService);
    private offerService: OfferService = Container.get<OfferService>(OfferService);
    private userOfferService: UserOfferService = Container.get<UserOfferService>(UserOfferService);

    @On([
        events.userOffer.updated,
    ])
    public async onUserOfferUpdated(data: any): Promise<void> {
        const authUser: AuthenticatedUser = data.authUser as AuthenticatedUser;
        const entity = data.content as UserOffer;
        switch (entity.status) {
            case OfferStatusType.REQUIREMENT_COMPLETED:
                    const offer = await entity.offer;
                    const offerRewards = await offer.rewards;
                    // start actual rewarding
                    break;
            default:
                break;
        }
    }
}
