/**
 * events
 * ---------------------
 * Define all your possible custom events here.
 */
export const events = {
    user: {
        created: 'onUserCreated',
        updated: 'onUserUpdated',
        deleted: 'onUserDeleted',
    },
    offer: {
        created: 'onOfferCreated',
        updated: 'onOfferUpdated',
        deleted: 'onOfferDeleted',
    },
    token: {
        created: 'onTokenCreated',
        updated: 'onTokenUpdated',
        deleted: 'onTokenDeleted',
    },
    entityLog: {
        created: 'onEntityLogCreated',
        updated: 'onEntityLogUpdated',
        deleted: 'onEntityLogDeleted',
    },
    requirementEvent: {
        created: 'onRequirementEventCreated',
        updated: 'onRequirementEventUpdated',
        deleted: 'onRequirementEventDeleted',
    },
    rewardEvent: {
        created: 'onRewardEventCreated',
        updated: 'onRewardEventUpdated',
        deleted: 'onRewardEventDeleted',
    },
    userOffer: {
        created: 'onUserOfferCreated',
        updated: 'onUserOfferUpdated',
        deleted: 'onUserOfferDeleted',
    },
    entity: {
        created: 'onEntityCreated',
        updated: 'onEntityUpdated',
        deleted: 'onEntityDeleted',
    },
};
