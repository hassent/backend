import { DEOCRATORREGISTRY } from '../../decorators/EntityDecorators';

export class Utils {

    public static isClassDecorated(decorator: string, className: string): boolean {
        return DEOCRATORREGISTRY[decorator] && DEOCRATORREGISTRY[decorator][className];
    }
}
