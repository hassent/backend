type IBuilder<T> = {
        [k in keyof T]: (arg: T[k], strict?: boolean) => IBuilder<T>
    }
    & {
    build(): T
};

export function Builder<T>(): IBuilder<T> {
    const built: any = {};

    const builder = new Proxy(
        {},
        {
            get(target: any, prop: any, receiver: any): any {
                if ('build' === prop) {
                    return () => built;
                }
                return (x: any, strict: boolean = true): any => {
                    if (x === undefined) {
                        if (strict === true) {
                            throw new Error(`${prop as string} is missing`);
                        }
                    } else {
                        built[prop] = x;
                    }
                    return builder;
                };
            },
        }
    );

    return builder as any;
}
