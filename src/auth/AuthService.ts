import * as express from 'express';
import { Container, Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { Token } from '../api/models/Token/TokenModel';
import { User } from '../api/models/User/UserModel';
import { UserRepository } from '../api/repositories/UserRepository';
import { TokenService } from '../api/services/TokenService';
import { UserService } from '../api/services/UserService';
import { Logger, LoggerInterface } from '../decorators/Logger';

@Service()
export class AuthService {

    private tokenService = Container.get<TokenService>(TokenService);
    private userService = Container.get<UserService>(UserService);

    constructor(
        @Logger(__filename) private log: LoggerInterface,
        @OrmRepository() private userRepository: UserRepository
    ) { }

    public parseBasicAuthFromRequest(req: express.Request): { username: string, token: string } {
        const authorization = req.header('authorization');

        if (authorization && authorization.split(' ')[0] === 'Basic') {
            this.log.info('Credentials provided by the client');
            const decodedBase64 = Buffer.from(authorization.split(' ')[1], 'base64').toString('ascii');
            const username = decodedBase64.split(':')[0];
            const token = decodedBase64.split(':')[1];
            if (username && token) {
                return { username, token };
            }
        }

        this.log.info('No credentials provided by the client');
        return undefined;
    }

    public parseTokenAuthFromRequest(req: express.Request): string {
        const token = req.header('api_key');

        if (token) {
            this.log.info('Credentials provided by the client');
            return token;
        }
        this.log.info('No credentials provided by the client');
        return undefined;
    }

    public async validateUser(username: string, password: string): Promise<User> {
        const user = await this.userRepository.findOne({
            where: {
                username,
            },
        });
        if (await User.comparePassword(user, password)) {
            return user;
        }

        return undefined;
    }

    public async validateUserToken(username: string, token: string): Promise<User> {
        const refDate = new Date();
        refDate.setHours(refDate.getHours() - 1);
        const user = await this.userService.findOneByOption({
            where: {
                username,
            },
        });
        if (!user) { return undefined; }
        const userToken = await this.userService.findOne(user.id).then(u => u).catch(e => undefined);
        if (!userToken || !Token.compareToken(userToken.token, token) || userToken.updated < refDate) {
            return undefined;
         }
        return Promise.resolve(user);
    }

}
