import { Request } from 'express';

import { User } from '../api/models/User/UserModel';

export class AuthenticatedUser {

    private DEFAULT_EMAIL = 'log@isheger.com';
    private DEFAULT_IP_ADDRESS = 'localhost';

    private _id: number;

    private _email: string;

    private _currentIpAddress: string;

    private _currentAuthToken: string;

    constructor(id?: number, email?: string, currentIpAddress?: string, currentAuthToken?: string) {
        this._id = id;
        this._email = email || this.DEFAULT_EMAIL;
        this._currentIpAddress = currentIpAddress || this.DEFAULT_IP_ADDRESS;
        this.currentAuthToken = currentAuthToken;
    }

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get email(): string {
        return this._email;
    }

    set email(email: string) {
        this._email = email;
    }

    get currentIpAddress(): string {
        return this._currentIpAddress;
    }

    set currentIpAddress(currentIpAddress: string) {
        this._currentIpAddress = currentIpAddress;
    }

    get currentAuthToken(): string {
        return this._currentAuthToken;
    }

    set currentAuthToken(currentAuthToken: string) {
        this._currentAuthToken = currentAuthToken;
    }

    public fromDBUserAndRequest(req: Request, user: User): AuthenticatedUser {
        this._id = user.id;
        this._email = user.email;
        this._currentIpAddress = req.ip;
        return this;
    }

}
