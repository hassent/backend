import { Action } from 'routing-controllers';
import { Container } from 'typedi';
import { Connection } from 'typeorm';

import { User } from '../api/models/User/UserModel';
import { env } from '../env';
import { Logger } from '../lib/logger';
import { AuthService } from './AuthService';

export function authorizationChecker(connection: Connection): (action: Action, roles: any[]) => Promise<boolean> | boolean {
    const log = new Logger(__filename);
    const authService = Container.get<AuthService>(AuthService);

    return async function innerAuthorizationChecker(action: Action, roles: string[]): Promise<boolean> {
        // here you can use request/response objects from action
        // also if decorator defines roles it needs to access the action
        // you can use them to provide granular access check
        // checker must return either boolean (true or false)
        // either promise that resolves a boolean value

        const credentials = authService.parseBasicAuthFromRequest(action.request);

        if (!credentials) {
            log.warn('No credentials given');
            return false;
        }

        if (action.request.url.includes('/authenticate')) {
            return credentials.token === env.dummyUser.secret;
        }

        const user: User = await authService.validateUserToken(credentials.username, credentials.token);
        if (user === undefined) {
            log.warn('Invalid credentials given');
            return false;
        }
        action.request.user = user.toAuthenticatedUser(action.request, credentials.token);
        log.info('Successfully checked credentials', action.request.user);
        return true;
    };
}
