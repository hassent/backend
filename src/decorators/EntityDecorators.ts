const DEOCRATORREGISTRY: {[decorator: string]: { [name: string]: any } } = {};

function Auditable(constructor: any): void {
    const decorator = 'Auditable';
    DEOCRATORREGISTRY[decorator] = DEOCRATORREGISTRY[decorator] || {};
    DEOCRATORREGISTRY[decorator][constructor.name] = constructor.name;
}

export { DEOCRATORREGISTRY, Auditable };
