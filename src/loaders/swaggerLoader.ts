import { getFromContainer, MetadataStorage } from 'class-validator';
import { validationMetadatasToSchemas } from 'class-validator-jsonschema';
import basicAuth from 'express-basic-auth';
import { MicroframeworkLoader, MicroframeworkSettings } from 'microframework-w3tec';
import { getMetadataArgsStorage } from 'routing-controllers';
import { routingControllersToSpec } from 'routing-controllers-openapi';
import * as swaggerUi from 'swagger-ui-express';

import { env } from '../env';

export const swaggerLoader: MicroframeworkLoader = (settings: MicroframeworkSettings | undefined) => {
    if (settings && env.swagger.enabled) {
        const expressApp = settings.getData('express_app');
        const metadatas = (getFromContainer(MetadataStorage) as any).validationMetadatas;
        const schemas = validationMetadatasToSchemas(metadatas, {
            refPointerPrefix: '#/components/schemas/',
        });

        const storage = getMetadataArgsStorage();
        const spec = routingControllersToSpec(storage, {
            controllers: env.app.dirs.controllers,
            routePrefix: env.app.routePrefix,
          }, {
                    components: {
                                schemas,
                                securitySchemes: {
                                    ApiKeyAuth: {
                                        type: 'apiKey',
                                        in: 'header',
                                        name: 'api_key',
                                    },
                                },
                            },
                    info: {
                        title: env.app.name,
                        description: env.app.description,
                        version: env.app.version,
                    },
                    servers : [
                        {
                            url: `${env.app.schema}://${env.app.host}:${env.app.port}`,
                        },
                    ],
                });
        expressApp.use(
            env.swagger.route,
            env.swagger.username ? basicAuth({
                users: {
                    [`${env.swagger.username}`]: env.swagger.password,
                },
                challenge: true,
            }) : (req, res, next) => next(),
            swaggerUi.serve,
            swaggerUi.setup(spec)
        );

    }
};
