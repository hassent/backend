import { MicroframeworkLoader, MicroframeworkSettings } from 'microframework-w3tec';
import { configure, format, transports } from 'winston';

import { env } from '../env';
import winston = require('winston');

export const winstonLoader: MicroframeworkLoader = (settings: MicroframeworkSettings | undefined) => {
    configure({
        transports: [
            new transports.Console({
                level: env.log.level,
                handleExceptions: true,
                format: env.node !== 'development'
                    ? format.combine(
                        format.json()
                    )
                    : format.combine(
                        format.colorize(),
                        format.simple()
                    ),
            }),
            new transports.File({
                filename: 'error.log',
                 level: 'error',
                 format: winston.format.combine(
                    winston.format.timestamp({
                      format: 'YYYY-MM-DD hh:mm:ss A ZZ',
                    }),
                    format.colorize(),
                    format.simple()
                  ),
                  handleExceptions: true }),
            new transports.File({
                filename: 'localhost.log',
                format: winston.format.combine(
                    winston.format.timestamp({
                      format: 'YYYY-MM-DD hh:mm:ss A ZZ',
                    }),
                    format.colorize(),
                    format.simple()
                  ),
                }),
        ],
    });
};
